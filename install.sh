#!/bin/bash
#
# Author: Ander

[[ $EUID -ne 0 ]] && echo "Please, run as root" && exit 1
INST_DIR=/usr/local/


echo "Installing myrandomplaylist..."
cp myrandomplaylist /tmp
sed -i "s:include:${INST_DIR}include:g" /tmp/myrandomplaylist
mv /tmp/myrandomplaylist ${INST_DIR}bin/
echo "Installing functions..."
cp include/* ${INST_DIR}include/
chmod 755 ${INST_DIR}bin/myrandomplaylist
echo "All done!"
